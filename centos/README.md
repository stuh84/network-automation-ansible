# Adding VLAN interfaces

Until NMCLI interaction is solved in Ansible, use the following to add VLAN interfaces: -

```
$ nmcli c add type vlan ifname VLAN$ID dev enp8s0 id $ID ipv4.addresses 192.168.$ID.254/24
```
